﻿using System;


class TextButton : Sprite
{
    Font font18;
    protected string text;
    protected bool selected;
    protected byte red;
    protected byte blue;
    protected byte green;

    public TextButton(String text, int x, int y, int width, int height, int fontSize = 10)
    {
        this.text = text;
        font18 = new Font("data/fonts/joystix.ttf", (short)fontSize);
        LoadImage("data/misc/black.png");
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        selected = false;
        red = blue = green;
    }

    public bool IsSelected() { return selected; }

    public bool CheckSelected(int x, int y)
    {
        if (
           this.x <= x
        && this.x + width >= x
        && this.y <= y
        && this.y + height >= y
    )
        {
            selected = true;
            red = 255;
            blue = green = 0;
        }
        else
        {
            selected = false;
            red = green = blue = 255;

        }
        return selected;
    }

    public void Draw()
    {
        SdlHardware.DrawHiddenImage(image, x, y, 0, 0, (short)width, (short)height);
        SdlHardware.WriteHiddenText(text, (short)(x + 5), (short)(y + 5), red, green, blue, font18);
    }

    public override void Move()
    {
        base.Move();
    }
}

