﻿using System.IO;
class MouseToPrinter
{
    public static short WIDTH = 600;
    public static short HEIGTH = 600;

    public static void CreateConf()
    {
        string confFile = 
            "#This is a sample using default settings. Rename to \"mtp.conf\"\n" +
            "#usage -> option:value\n" +
            "#All settings are optional\n" +
            "serialPort:/dev/ttyUSB0\n" +
            "baudrate:115200\n" +
            "maxWidth:210\n" +
            "maxHeigth:210\n" +
            "xOffset:5\n" +
            "yOffset:5\n" +
            "zLift:0.8\n" +
            "zOffset:10\n" +
            "feedrate:3600\n" +
            "minFeedrate:900\n" +
            "maxFeedrate:7200\n" +
            "maxPause:150\n" +
            "minPause:16\n" +
            "pauseInterval:50";
        File.WriteAllText("mtp.conf.sample",confFile);
    }

    public static void Main(string[] args)
    {

        if (args.Length >= 1 && (args[0] == "-s"))
        {
            CreateConf();
        }
        else if(args.Length >= 1 && (args[0] == "-h"))
        {
            System.Console.WriteLine(
                "This software translate mouse " +
                 "clicks and movement to a 3D printer in real time.");
            System.Console.WriteLine("-s -> Creates a sample config file");
            System.Console.WriteLine("-h -> Shows this help");
            System.Console.WriteLine("-v -> Verbose output, show all gcodes sended");
        }
        else
        {
            bool fullScreen = false;
            SdlHardware.Init(WIDTH, HEIGTH, 24, fullScreen);
            MouseDetection m = new MouseDetection(args.Length >= 1 && (args[0] == "-v"));
            m.Run();
        }
    }
}

