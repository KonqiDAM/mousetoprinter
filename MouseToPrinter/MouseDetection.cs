﻿using System;
using System.IO.Ports;
using System.IO;

public class MouseDetection
{
    bool continueSendingCommands;
    bool verbose = false;
    bool isLifed;
    string lineToSend;
    string serialPort;
    int baudrate;
    int maxWidth;
    int xOffset;
    int maxHeigth;
    int yOffset;
    int pauseInterval;
    int minPause;
    int maxPause;
    double feedrate;
    double maxFeedrate;
    double minFeedrate;
    double zOffset;
    double zLift;
    double xMult;
    double yMult;

    Image pixel;

    SerialPort COM1;

    public MouseDetection(bool verbose)
    {
        this.verbose = verbose;
        pixel = new Image("pixel.png");
        continueSendingCommands = true;
        serialPort = "/dev/ttyUSB0";
        isLifed = true;
        baudrate = 115200;
        maxWidth = 200;
        maxHeigth = 200;
        xOffset = 5;
        yOffset = 5;
        zLift = 0.8;
        zOffset = 10;
        feedrate = 3600; // = speed, not related with extruder
        minFeedrate = 900;
        maxFeedrate = 7200;
        maxPause = 150;
        minPause = 16;
        pauseInterval = 50;

        xMult = (double)maxWidth / MouseToPrinter.WIDTH;
        yMult = (double)maxHeigth / MouseToPrinter.HEIGTH;

        COM1 = new SerialPort(serialPort, 115200);
        COM1.Open();
        SdlHardware.Pause(2000);
        COM1.WriteLine("G28");
        COM1.WriteLine("G1 Z" + (zOffset + zLift));
        COM1.WriteLine("G1 F" + feedrate);
        if(verbose)
            Console.WriteLine("Connected to: " + "\nG28\nG1 Z"+(zOffset + zLift)+"\nG1 F"+feedrate);
    }

    public void LoadSettings()
    {

        if(!File.Exists("mtp.conf"))
        {
            Console.WriteLine(
                "Configuration file not found, consider create one" +
                " with \"-s\".");
        }
        else
        {
            try
            {
                StreamReader readFile = new StreamReader("mtp.conf");
                string line = readFile.ReadLine();
                while(line != null)
                {
                    if (line.StartsWith("#"))
                        continue;
                    switch(line.Split(':')[0])
                    {
                        case "baudrate":
                            baudrate = Convert.ToInt32(line.Split(':')[1]);
                            break;

                        case "serialPort":
                            serialPort = line.Split(':')[1];
                            break;

                        case "maxWidth":
                            maxWidth = Convert.ToInt32(line.Split(':')[1]);
                            break;

                        case "maxHeigth":
                            maxHeigth = Convert.ToInt32(line.Split(':')[1]);
                            break;

                        case "xOffset":
                            xOffset = Convert.ToInt32(line.Split(':')[1]);
                            break;

                        case "yOffset":
                            yOffset = Convert.ToInt32(line.Split(':')[1]);
                            break;

                        case "zLift":
                            zLift = Convert.ToInt32(line.Split(':')[1]);
                            break;

                        case "zOffset":
                            zOffset = Convert.ToInt32(line.Split(':')[1]);
                            break;

                        case "maxFeedrate":
                            maxFeedrate = Convert.ToInt32(line.Split(':')[1]);
                            break;

                        case "minFeedrate":
                            minFeedrate = Convert.ToInt32(line.Split(':')[1]);
                            break;

                        case "feedrate":
                            feedrate = Convert.ToInt32(line.Split(':')[1]);
                            break;

                        case "maxPause":
                            maxPause = Convert.ToInt32(line.Split(':')[1]);
                            break;

                        case "minPause":
                            minPause = Convert.ToInt32(line.Split(':')[1]);
                            break;

                        case "pauseInterval":
                            pauseInterval = Convert.ToInt32(line.Split(':')[1]);
                            break;
                    }
                }

                readFile.Close();
            } catch (IOException e)
            {
                Console.WriteLine("Error on file reading!: " + e.Message);
            } catch (Exception e)
            {
                Console.WriteLine("Error on settings!: " + e.Message);
            }
        }
    }

    public void KeyBoardInput()
    {
        continueSendingCommands &= !SdlHardware.KeyPressed(SdlHardware.KEY_ESC);

        if (SdlHardware.KeyPressed(SdlHardware.KEY_RIGHT))
        {
            feedrate += 1000;
            if (feedrate > maxFeedrate)
                feedrate = maxFeedrate;
            COM1.WriteLine("G1 F1" + feedrate);
            if (verbose)
                Console.WriteLine("G1 F1" + feedrate);
        }

        if (SdlHardware.KeyPressed(SdlHardware.KEY_LEFT))
        {
            feedrate -= 1000;
            if (feedrate < minFeedrate)
                feedrate = minFeedrate;
            COM1.WriteLine("G1 F1" + feedrate);
            if (verbose)
                Console.WriteLine("G1 F1" + feedrate);
        }

        if (SdlHardware.KeyPressed(SdlHardware.KEY_UP))
        {
            pauseInterval -= 10;
            if (pauseInterval < minPause)
                pauseInterval = minPause;
            if(verbose)
                Console.WriteLine("Pause set to: " +pauseInterval);
        }

        if (SdlHardware.KeyPressed(SdlHardware.KEY_DOWN))
        {
            pauseInterval += 10;
            if (pauseInterval > maxPause)
                pauseInterval = maxPause;
            if(verbose)
                Console.WriteLine("Pause set to: " + pauseInterval);

        }
    }

    public void DetectMouse()
    {

        if (SdlHardware.MouseClicked(1))
        {
            SdlHardware.DrawHiddenImage(pixel,
                                        SdlHardware.GetMouseX(),
                                        SdlHardware.GetMouseY());

            if (isLifed)
            {
                isLifed = false;
                Console.WriteLine("G1 Z" + zOffset);
                COM1.WriteLine("G1 Z" + zOffset);
            }


            lineToSend = "G1 X" +
                ((1 + SdlHardware.GetMouseX()) * xMult 
                 + xOffset).ToString("#.00") + " Y" +
                ((1 + SdlHardware.GetMouseY() * yMult) 
                 + yOffset).ToString("#.00");
            
            COM1.WriteLine(lineToSend);
            if (verbose)
                Console.WriteLine(lineToSend);
        }
        else if (!isLifed)
        {
            isLifed = true;
            Console.WriteLine("G1 Z" + (zOffset + zLift));
            COM1.WriteLine("G1 Z" + (zOffset + zLift));
        }
    }

    public void DrawScreen()
    {
        SdlHardware.ShowHiddenScreen();
    }

    public void Run()
    {
        while (continueSendingCommands)
        {
            DetectMouse();
            KeyBoardInput();
            DrawScreen();
            SdlHardware.Pause(pauseInterval);
        }
        COM1.WriteLine("M18");
        if (verbose)
            Console.WriteLine("M18");
        COM1.Close();
    }
}

